package com.jzbrooks.kino;

import android.os.Parcel;
import android.os.Parcelable;

import java.net.URL;

public class KinoItem implements Parcelable {

    private String title;
    private String description;
    private int rating;
    private URL posterImageUrl;
    private int reviewRating;

    public KinoItem() {

    }

    protected KinoItem(Parcel in) {
        title = in.readString();
        description = in.readString();
        rating = in.readInt();
        reviewRating = in.readInt();
    }

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() { return description; }
    public void setDescription(String description) {
        this.description = description;
    }

    public int getRating() {
        return rating;
    }
    public void setRating(int rating) {
        this.rating = rating;
    }

    public URL getPosterImageUrl() {
        return posterImageUrl;
    }
    public void setPosterImageUrl(URL posterImageUrl) {
        this.posterImageUrl = posterImageUrl;
    }

    public int getReviewRating() {
        return reviewRating;
    }
    public void setReviewRating(int reviewRating) {
        this.reviewRating = reviewRating;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(title);
        parcel.writeString(description);
        parcel.writeInt(rating);
        parcel.writeInt(reviewRating);
    }

    public static final Creator<KinoItem> CREATOR = new Creator<KinoItem>() {
        @Override
        public KinoItem createFromParcel(Parcel in) {
            return new KinoItem(in);
        }

        @Override
        public KinoItem[] newArray(int size) {
            return new KinoItem[size];
        }
    };
}
