package com.jzbrooks.kino;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PosterFeedAdapter extends RecyclerView.Adapter<PosterFeedAdapter.PosterViewHolder> {

    private Activity mHost;
    private Context mContext;
    private OnPosterClickedListener mListener;
    private List<KinoItem> mItems;

    public PosterFeedAdapter(Activity hostActivity, OnPosterClickedListener listener) {
        this.mHost = hostActivity;
        this.mListener = listener;
        this.mContext = hostActivity.getApplicationContext();
        this.mItems = new ArrayList<>();
    }

    @Override
    public PosterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);

        View view = inflater.inflate(R.layout.poster_feed_item, parent, false);

        final PosterViewHolder holder = new PosterViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(PosterViewHolder holder, final int position) {
        holder.bind(mItems.get(position), mListener);
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public void addItem(KinoItem item) {
        mItems.add(item);
        notifyItemInserted(mItems.size()-1);
    }

    public boolean contains(KinoItem item) {
        boolean result = false;

        for(KinoItem currItem : mItems) {
            if (currItem.getTitle().equals(item.getTitle()))
                return true;
        }

        return result;
    }

    public void clear() {
        int size = mItems.size();

        mItems.clear();
        notifyItemRangeRemoved(0, size);
    }

    public class PosterViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.poster_view) ImageView mImage;

        public PosterViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }

        public void bind(final KinoItem kinoItem, final OnPosterClickedListener listener) {
            String imageUrl = kinoItem.getPosterImageUrl().toString();

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mListener.onPosterClicked(PosterViewHolder.this, kinoItem);
                }
            });

            Glide.with(mContext)
                    .load(imageUrl)
                    .fitCenter()
                    .crossFade()
                    .into(mImage);
        }
    }
}
