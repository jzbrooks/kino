package com.jzbrooks.kino;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.graphics.ColorUtils;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.graphics.Palette;
import android.transition.TransitionManager;
import android.util.DisplayMetrics;
import android.util.Property;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import java.util.concurrent.ExecutionException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static android.graphics.Color.TRANSPARENT;


public class DetailsFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";

    @BindView(R.id.details_image_banner)
    ImageView mBanner;
    @BindView(R.id.details_text_title)
    TextView mTitle;
    @BindView(R.id.details_text_description)
    TextView mDescription;
    @BindView(R.id.bottom_sheet)
    NestedScrollView mBottomSheet;

    private BottomSheetBehavior mBottomSheetBehavior;
    private Unbinder mUnbinder;
    private KinoItem mKinoItem;

    public DetailsFragment() {

    }

    public static DetailsFragment newInstance(KinoItem item) {
        DetailsFragment fragment = new DetailsFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM1, item);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mKinoItem = getArguments().getParcelable(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_details, container, false);
        mUnbinder = ButterKnife.bind(this, view);

        ((AppCompatActivity)getActivity()).getSupportActionBar().setBackgroundDrawable(new ColorDrawable(TRANSPARENT));

        Glide.with(this)
                .load(mKinoItem.getPosterImageUrl().toString())
                .asBitmap()
                .centerCrop()
                .into(new BitmapImageViewTarget(mBanner) {
                    @Override
                    public void onResourceReady(Bitmap bitmap, GlideAnimation anim) {
                        super.onResourceReady(bitmap, anim);
                        Palette.Builder palette = new Palette.Builder(bitmap);
                        palette.generate(new Palette.PaletteAsyncListener() {
                            @Override
                            public void onGenerated(Palette palette) {
                                int defaultColor = getResources().getColor(R.color.colorPrimaryDark);
                                int color = palette.getVibrantColor(defaultColor);
                                if (color == defaultColor) {
                                    color = palette.getLightVibrantColor(defaultColor);
                                    if (color == defaultColor) {
                                        color = palette.getLightMutedColor(defaultColor);
                                        if (color == defaultColor) {
                                            color = palette.getMutedColor(defaultColor);
                                        }
                                    }
                                }

                                float[] hsl = new float[3];
                                ColorUtils.colorToHSL(color, hsl);
                                if (0.5 > hsl[2]) {
                                    mTitle.setTextColor(0xFAFAFAFF);
                                }

                                ObjectAnimator.ofObject(mTitle, "backgroundColor", new ArgbEvaluator(), 0xfafafa, color)
                                        .setDuration(200)
                                        .start();
                            }
                        });
            }
        });

        mTitle.setText(mKinoItem.getTitle());
        mDescription.setText(mKinoItem.getDescription());
        mBottomSheetBehavior = BottomSheetBehavior.from(mBottomSheet);

        mTitle.measure(0,0);
        int peekHeight = calculatePeekHeight(mTitle.getMeasuredHeight());
        mBottomSheetBehavior.setPeekHeight(peekHeight);
        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        mUnbinder.unbind();
    }

    public int calculatePeekHeight(int titleHeight) {
        int navigationBarHeight = 0;
        int resourceId = getResources().getIdentifier("navigation_bar_height", "dimen", "android");
        if (resourceId > 0) {
            navigationBarHeight = getResources().getDimensionPixelSize(resourceId);
        }

        int widthMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.AT_MOST);
        int heightMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        mTitle.measure(widthMeasureSpec, heightMeasureSpec);

        DisplayMetrics metrics = getResources().getDisplayMetrics();
        return navigationBarHeight + titleHeight;
    }
}
