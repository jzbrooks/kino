package com.jzbrooks.kino;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Parcel;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.animation.FastOutLinearInInterpolator;
import android.support.v4.view.animation.LinearOutSlowInInterpolator;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.ViewUtils;
import android.transition.Explode;
import android.transition.TransitionInflater;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowInsets;
import android.widget.FrameLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment {

    @BindView(R.id.poster_grid)
    public RecyclerView mPosterFeed;

    private PosterFeedAdapter mPosterFeedAdapter;
    private FragmentManager mFragmentManager;
    private Unbinder mUnbinder;

    public MainActivityFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        mFragmentManager = getFragmentManager();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.menu_fragment_poster_feed, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.action_refresh) {
            mPosterFeedAdapter.clear();
            FetchMoviesTask fetchMoviesTask = new FetchMoviesTask();
            fetchMoviesTask.execute(mPosterFeedAdapter.getItemCount());
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_main, container, false);

        mUnbinder = ButterKnife.bind(this, view);

        mPosterFeedAdapter = new PosterFeedAdapter(getActivity(), new OnPosterClickedListener() {
            @Override
            public void onPosterClicked(PosterFeedAdapter.PosterViewHolder holder, KinoItem item) {
                setSharedElementReturnTransition(new PosterTransition());
                setExitTransition(TransitionInflater.from(getActivity()).inflateTransition(android.R.transition.explode));


                DetailsFragment fragment = DetailsFragment.newInstance(item);

                fragment.setSharedElementEnterTransition(new PosterTransition().setInterpolator(new LinearOutSlowInInterpolator()));

                ViewCompat.setTransitionName(holder.mImage, item.getTitle() + "_poster_transition");

                mFragmentManager.beginTransaction()
                        .replace(R.id.fragment, fragment)
                        .addToBackStack("detail_fragment")
                        .addSharedElement(holder.mImage, item.getTitle() + "_poster_transition")
                        .commit();
            }
        });

        GridLayoutManager gridLayoutManager = new GridLayoutManager(this.getContext(), 2);

        mPosterFeed.setAdapter(mPosterFeedAdapter);
        mPosterFeed.setLayoutManager(gridLayoutManager);
        mPosterFeed.addOnScrollListener(new EndlessScrollListener(gridLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                FetchMoviesTask task = new FetchMoviesTask();
                task.execute(page);
            }
        });

        //get the first set of pages and let the endlessscrollview take over
        FetchMoviesTask task = new FetchMoviesTask();
        task.execute(1);

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        mUnbinder.unbind();
    }

    public class FetchMoviesTask extends AsyncTask<Integer, Void, KinoItem[]> {

        private String LOG_TAG = this.getClass().getSimpleName();

        @Override
        protected KinoItem[] doInBackground(Integer... params) {

            // These two need to be declared outside the try/catch
            // so that they can be closed in the finally block.
            OkHttpClient client = new OkHttpClient();

            // Will contain the raw JSON response as a string.
            String movieJsonStr = null;

            try {
                // Construct the URL for the The Movie Database query
                // Possible parameters are available at TMDB's API page, at
                final String MOVIES_BASE_URL =
                        "http://api.themoviedb.org/3/discover/movie?";

                final String PAGE_PARAM = "page";
                final String SORT_PARAM = "sort_by";
                final String START_DATE_PARAM = "primary_release_year";
                final String API_KEY_PARAM = "api_key";

                int page = params[0];

                String timeStamp = new SimpleDateFormat("yyyy").format(Calendar.getInstance().getTime());


                Uri builtUri = Uri.parse(MOVIES_BASE_URL).buildUpon()
                        .appendQueryParameter(PAGE_PARAM, Integer.toString(page))
                        .appendQueryParameter(SORT_PARAM, "popularity.desc")
                        .appendQueryParameter(API_KEY_PARAM, "cbb2c7d75d6ef235594df10eebd57ceb")
                        .build();

                URL url = new URL(builtUri.toString());
                Log.v(LOG_TAG, "Built URI " + builtUri.toString());

                Request request = new Request.Builder()
                        .url(url)
                        .build();

                Response response = client.newCall(request).execute();
                movieJsonStr = response.body().string();

                Log.v(LOG_TAG, "TMDB JSON Response: " + movieJsonStr);
            } catch (IOException e) {

                Log.e(LOG_TAG, "Error ", e);
                // no point in attempting to parse the response if we don't have a well formed response
                return null;
            }

            try {

                return getKinoItemsFromJson(movieJsonStr);

            } catch (JSONException e) {
                Log.e(LOG_TAG, e.getMessage(), e);
                e.printStackTrace();
            } catch (Exception e) {

                Log.e(LOG_TAG, e.getMessage(), e);
                e.printStackTrace();

            }

            // This will only happen if there was an error getting or parsing the movies.
            return null;
        }

        @Override
        protected void onPostExecute(KinoItem[] result) {
            if (result != null) {
                for(KinoItem kinoItem : result) {

                    if (!mPosterFeedAdapter.contains(kinoItem)) {
                        mPosterFeedAdapter.addItem(kinoItem);
                    }
                }
            }
        }

        private KinoItem[] getKinoItemsFromJson(String tmdbJson)
                throws JSONException {

            // These are the names of the JSON objects that need to be extracted.
            final String TMDB_LIST = "results";
            final String TMDB_TITLE = "original_title";
            final String TMDB_OVERVIEW = "overview";
            final String TMDB_POSTERURL = "poster_path";
            final String TMDB_APIKEY = "api_key";

            final String TMDB_BASE_IMG_URL =
                    "http://image.tmdb.org/t/p/w500/";

            JSONObject moviesJson = new JSONObject(tmdbJson);
            JSONArray movieArray = moviesJson.getJSONArray(TMDB_LIST);
            int jsonArraySize = movieArray.length();

            KinoItem[] parsedItems = new KinoItem[jsonArraySize];
            for(int i = 0; i < jsonArraySize; i++) {
                KinoItem kinoItem = new KinoItem();

                // Get the JSON object representing the day
                JSONObject movieJsonObject = movieArray.getJSONObject(i);

                kinoItem.setTitle(movieJsonObject.getString(TMDB_TITLE));
                kinoItem.setDescription(movieJsonObject.getString(TMDB_OVERVIEW));

                try {

                    String posterImgPath = movieJsonObject.getString(TMDB_POSTERURL);
                    Uri builtUri = Uri.parse(TMDB_BASE_IMG_URL).buildUpon()
                            .appendEncodedPath(posterImgPath.replace("\\", ""))
                            .appendQueryParameter(TMDB_APIKEY, "cbb2c7d75d6ef235594df10eebd57ceb")
                            .build();

                    URL url = new URL(builtUri.toString());
                    kinoItem.setPosterImageUrl(url);

                } catch (MalformedURLException ex) {
                    Log.d(LOG_TAG, "Poster URL is malformed.");
                }

                parsedItems[i] = kinoItem;
            }

            for (KinoItem kinoItem : parsedItems) {
                Log.v(LOG_TAG, "Movie entry: " + kinoItem.getTitle());
            }

            return parsedItems;
        }
    }
}
