package com.jzbrooks.kino;


import android.support.v7.widget.RecyclerView;

public interface OnPosterClickedListener {
    void onPosterClicked(PosterFeedAdapter.PosterViewHolder holder, KinoItem item);
}
