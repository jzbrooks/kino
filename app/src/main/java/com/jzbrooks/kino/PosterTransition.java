package com.jzbrooks.kino;

import android.transition.ArcMotion;
import android.transition.ChangeBounds;
import android.transition.ChangeClipBounds;
import android.transition.ChangeImageTransform;
import android.transition.ChangeTransform;
import android.transition.Explode;
import android.transition.PathMotion;
import android.transition.Transition;
import android.transition.TransitionSet;

public class PosterTransition extends TransitionSet {
    public PosterTransition() {
        setOrdering(ORDERING_TOGETHER);

        ArcMotion motion = new ArcMotion();

        addTransition(new ChangeBounds())
                .addTransition(new ChangeTransform())
                .addTransition(new ChangeClipBounds())
                .addTransition(new ChangeImageTransform())
                .setPathMotion(motion);
    }
}
